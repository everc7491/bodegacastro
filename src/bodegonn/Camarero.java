package bodegonn;

public class Camarero extends Persona{
private Menu m;
private Pedidos c;

    public Camarero(String usuario, String password, String cargo) {
        super(usuario, password, cargo);
        setCargo("Camarero");
     
    }
   

    @Override
    public boolean proceder() {
         boolean cerrar = false, seg=true;;
         String comanda;
         float cuenta=0;
       String u=usuario;
         int mesa,cant,i;
        while (!cerrar) {
            int op;
          
              op = EntradaSalida.leerInt("MENÚ CAMARERO\n\n"
                        + "[0] Cerrar Sesión\n"
                        + "[1] GENERAR COMANDA");
              

            if (op==0){
                EntradaSalida.mostrarString("Hasta luego!!!");
                cerrar=true;
            }else if(!(m.obtenerProductosDisponibles().equals(""))){
                         mesa=EntradaSalida.leerInt("Nº de Mesa:");
                         comanda="MESA: "+mesa;
                         do{
                             do{
                                   i=EntradaSalida.leerInt("Ingrese el codigo del pedido:\n"                                    
                               + "\n " + m.obtenerProductosDisponibles());
                               } while(!existeCodDisp(i));
                            cant=EntradaSalida.leerInt("Cantidad: ");
                             comanda+="\n"+ cant+ " "+ m.obtenerNombreProducto(i);
                             cuenta+=m.obtenerPrecio(i)*cant;
                       //      pe.sumarCant(u);
                             m.sumar(i,cant);
                            
                          seg=EntradaSalida.leerBoolean("¿Agregrar otro articulo al pedido?");
                          }while(seg);
                comanda+="\n"+u; 
          
                Comanda com= new Comanda(comanda,cuenta, u);
                cuenta=0;
                c.agregar(com);
             
            }else{
            EntradaSalida.mostrarString("No hay menu disponible!!!");}
            
        }
        
       return true;
    }
   
     boolean existeCodDisp(int buscar){
    boolean r=false;
    int aux;
 
        for( int i=0;i<m.obtenerCodDisp().size();i++){
            aux=(int) m.obtenerCodDisp().get(i);
          if(buscar==aux){
              r=true;
          }
        }
    
    return r;
    
    }
    public Menu getM() {
        return m;
    }

    public void setC(Pedidos c) {
        this.c = c;
    }


    public Pedidos getC() {
        return c;
    }

    public void setM(Menu m) {
        this.m = m;
    }

  

}
