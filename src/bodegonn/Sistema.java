package bodegonn;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;


public class Sistema implements Serializable{

    private Menu m;
    private Personal pe;
    private Pedidos c;

    public Sistema() {
        m = new Menu();
        c = new Pedidos();
        pe = new Personal();
    }
    public Sistema deSerializar(String archivo) throws IOException, ClassNotFoundException {
        FileInputStream f = new FileInputStream(archivo);
        ObjectInputStream o = new ObjectInputStream(f);
        Sistema s = (Sistema) o.readObject();
        return s;
    }

     public void serializar(String archivo) throws IOException {
        FileOutputStream f = new FileOutputStream(archivo);
        ObjectOutputStream o = new ObjectOutputStream(f);
        o.writeObject(this);
        o.close();
    }
    void arrancar() {
         boolean corriendo = true;

        while (corriendo) {
          

            String usuario = EntradaSalida.leerString("Ingrese su usuario");
            String password = EntradaSalida.leerPassword("Ingrese password");
            Persona u=pe.buscar(usuario,password);
            

            if (u == null) {
                EntradaSalida.mostrarString("Usuario/contraseña inexistente");
            } else {
          
                corriendo = u.proceder();
            }
        }
        EntradaSalida.mostrarString("Hasta mañana\n"
                + "gracias por utilizar Sistema de CASTRO,HEBER");
    }

     public void inicializacion() {
        String usuario, password;
        EntradaSalida.mostrarString(
                "PRIMER ARRANQUE\n\n"
                + "A continuación genere el Administrador para el sistema.");

        usuario = EntradaSalida.leerString("Nombre de usuario del Administrador.");
        password = EntradaSalida.leerPassword("Contraseña del Administrador.");
        Administrador a = new Administrador(usuario, password,"Administrador");
        a.setM(m);
        a.setC(c);
        a.setPe(pe);
        pe.agregarAdministrador(a);
       
        EntradaSalida.mostrarString(
               
                "USUARIO CARGADO\n\n"
                + "Ya puede ingresar al sistema.");
    }

    public Menu getM() {
        return m;
    }

    public void setM(Menu m) {
        this.m = m;
    }
    public Pedidos getC() {
        return c;
    }

    public void setC(Pedidos c) {
        this.c = c;
    }

    public Personal getPe() {
        return pe;
    }

    public void setPe(Personal pe) {
        this.pe = pe;
    }
    
}
