package bodegonn;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Pedidos implements Serializable{
    private List<Comanda> comandas;

    public Pedidos() {
        comandas = new ArrayList<>();
    }   
    public void agregar(Comanda com) {
        if (com != null) {
            comandas.add(com);
            EntradaSalida.mostrarString(com.getPedido()); 
        }
   
}
    public List<Comanda> getComandas() {
        return comandas;
    }

    float facturacion() {
       float a=0;
       Comanda co=null;
       if(comandas!=null){
            for(int i=0;i<comandas.size();i++){
                co=comandas.get(i);
            a+=co.getCuenta();
            }
       }
        return a;
    }

    String masPedidos() {
        String mas="", nom="";
      
        int maximo=0,pedidos=0;
        if(comandas != null){ 
         
            
          for(int i=0; i<comandas.size();i++){
              nom=comandas.get(i).getCamarero();
              for(int j=0; j<comandas.size();j++){
                  if(comandas.get(j).getCamarero().equals(nom)){
                   pedidos++;
               } 
              }
              if(maximo<pedidos){
                  mas=nom+" con "+ pedidos;
              }
             pedidos=0; 
            }      
      }else{
            mas="NO hay Datos Suficientes";
        }
        return mas;
    }

   
}