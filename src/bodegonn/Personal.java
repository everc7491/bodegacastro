package bodegonn;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Personal implements Serializable{
      private List<Persona> personas;

    public Personal() {
        personas = new ArrayList<>();
    }  

    void agregarCamarero(Camarero a) {
        if(a!=null){
        personas.add(a);
        }
    }
     void agregarCocinero(Cocinero a) {
        if(a!=null){
        personas.add(a);
        }
    }
      void agregarAdministrador(Administrador a) {
        if(a!=null){
        personas.add(a);
        }
    }

    Persona buscar(String usuario, String password) {
        Persona u=null;
        for (Persona p : personas) {
                if (p.getUsuario().equals(usuario) && p.getPassword().equals(password)) {
                    u = p;
               
                }
            }
        return u;
    }
boolean verificarUsuario(String usu){
    boolean verificacion=false;
     for (Persona p : personas) {
                if (p.getUsuario().equals(usu)) {
                    verificacion=true;
               
                }
            }
    return verificacion;
} 
   
}
