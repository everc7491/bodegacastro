package bodegonn;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Menu implements Serializable{
  private List<Producto> productos;
  
    public Menu() {
        productos = new ArrayList<>();
    }   

    public void setProductos(List<Producto> productos) {
        this.productos = productos;
    }

    String obtenerProductosDisponibles() {
        String lista = "";
        Producto pro=null;
        for (int i = 0; i < productos.size(); i++) {
            pro=productos.get(i); 
            if((pro.getPrecio())!=0){
           lista += "[" + i + "] " + pro.getNombre() + "$"+pro.getPrecio()+"\n";
            }
        }
        return lista;
    
        
        
        
    }

    public String obtenerNombreProducto(int i) {
        return productos.get(i).getNombre();
    }
    public float obtenerPrecio(int i){
        return productos.get(i).getPrecio();
    }
    void agregar(Preparacion pre) {
        if (pre!=null){
            productos.add(pre);
        }
    }

    void sumar(int i, int cant) {
     int total=productos.get(i).getCantidad();
     productos.get(i).setCantidad(total+cant);
    }

    String preparacionMasPedida() {
          boolean hayPrep=false;
          String inf="";
       int maximo=0;
         if(productos!=null){
                for (Producto p : productos) {
                   
                    if(p.getTipo().equals("PREPARACION")){
                        hayPrep=true;
                         if(p.getCantidad()>maximo){
                            maximo=p.getCantidad();
                            inf=" "+p.getNombre()+" con "+p.getCantidad();
                          }
                     }
                }
               if(hayPrep==false){
                   inf= "No hay PREPARACIONES cargadas";
               }
         } else{
            inf= "No hay productos disponibles";
         }
       return inf;
    }
    
    

    void agregarBebida(Bebida nuevoBeb) {
       productos.add(nuevoBeb);
    }

    String obtenerPreparacionesNoDisponibles() {
       String lista = "";
       Producto pro=null;
        if(productos!=null){     
               for (int i = 0; i < productos.size(); i++) {
               pro=productos.get(i);
                   if((pro.getPrecio())==0){
                      lista += "[" + i + "] " + pro.getNombre() +"\n";
                    }
                } 
        }
        return lista;
    }
    ArrayList obtenerCodNoDisp(){
        
    ArrayList<Integer> num=new ArrayList<Integer>();
    Producto pro=null;
        if(productos!=null){     
               for (int i = 0; i < productos.size(); i++) {
               pro=productos.get(i);
                   if((pro.getPrecio())==0){
                     num.add(i);
                    }
                } 
        }
    return num;
    }
       ArrayList obtenerCodDisp(){
        
    ArrayList<Integer> num=new ArrayList<Integer>();
    Producto pro=null;
        if(productos!=null){     
               for (int i = 0; i < productos.size(); i++) {
               pro=productos.get(i);
                   if((pro.getPrecio())!=0){
                     num.add(i);
                    }
                } 
        }
    return num;
    }
    void precioPreparacion(float pre,int i){
        Producto prep=null;
        prep=productos.get(i);
        prep.setPrecio(pre);
        productos.set(i, prep);
    
   
    }

 

    String bebidaMasPedida() {
       boolean hayBebidas=false;
       int maximo=0;
       String inf="";
    
        if(productos!=null){
               for (Producto pro : productos) {
                    if(pro.getTipo().equals("BEBIDA")){
                        hayBebidas=true;
                         if(pro.getCantidad()>maximo){
                         maximo=pro.getCantidad();
                         inf=pro.getNombre();
                         }
                     }
                }
               if(hayBebidas==false){
                   inf= "No hay Bebidas cargadas";
               }
         } else{
            inf= "No hay productos disponibles";
         }
       return inf;
    }
    void reestablecerCantidad(){
        for(int i=0;i< productos.size();i++){
            productos.get(i).setCantidad(0);
        }
    }
}
