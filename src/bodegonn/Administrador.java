package bodegonn;

public class Administrador extends Persona{
private Menu m;
private Pedidos c;
private Personal pe;

    public Administrador(String usuario, String password, String cargo) {
        super(usuario, password, cargo);
        setCargo("Administrador");
    }
   

    @Override
    public boolean proceder() {
       boolean seguirCorriendo = true;
        boolean cerrar = false;
        String pas="";
        String usu="", nomProd="",prep="1",beb="1";
        float precio=0;
        int i;
        while (!cerrar) {
            int op;
            do {
                op = EntradaSalida.leerInt(
                        "MENÚ ADMINISTRADOR\n\n"
                        + "[0] Salir\n"
                        + "[1] Cerrar Sesión\n"
                        + "[2] Alta Camarero\n"
                        + "[3] Alta Cocinero\n"
                        + "[4] Alta Bebida\n"
                        + "[5] Precio Preparacion\n"
                        + "[6] Informe\n");
            } while (op < 0 || op > 6);
                 switch(op){
                     case 1:
                EntradaSalida.mostrarString("¡Hasta pronto!");
                         cerrar = true;
                         break;
                     case 0:     
                         cerrar = true;
                         seguirCorriendo = false;
                         break;
                     case 2:
                         do{
                            usu = EntradaSalida.leerString("Nombre de nuevo usuario: ");
                            if(pe.verificarUsuario(usu)){
                               EntradaSalida.mostrarString("Usuario ["+usu+" ya existe.");
                            }
                         }while(pe.verificarUsuario(usu));
                            pas = EntradaSalida.leerPassword("Contraseña del Nuevo usuario:");
                            altaCamarero(usu,pas);
                         break;
                     case 3:
                          do{
                            usu = EntradaSalida.leerString("Nombre de nuevo usuario: ");
                            if(pe.verificarUsuario(usu)){
                               EntradaSalida.mostrarString("Usuario ["+usu+"] ya existe..");
                            }
                         }while(pe.verificarUsuario(usu));
                            pas = EntradaSalida.leerPassword("Contraseña del Nuevo usuario:");
                            altaCocinero(usu,pas);
                            
                         break;
                     case 4:
                         nomProd=EntradaSalida.leerString("Nombre de la Bebida: ");
                         precio=EntradaSalida.leerFloat("Precio de la Bebida: ");
                         altaBebida(nomProd,precio);
                         break;
                     case 5:
                         if(!(m.obtenerPreparacionesNoDisponibles().equals(""))){
                             do{
                                   i=EntradaSalida.leerInt("Ingrese el codigo del producto:\n"
                                   + m.obtenerPreparacionesNoDisponibles());
                             }while(!existeCodNoDisp(i));
                              precio=EntradaSalida.leerFloat("Ingrese el precio de la preparacion: $");
                               m.precioPreparacion(precio, i);
                            } else{
                         EntradaSalida.mostrarString("No hay preparaciones Disponibles!");
                         }
                         break;
                     case 6:
                         beb=m.bebidaMasPedida();
                         prep=m.preparacionMasPedida();
                        
                         EntradaSalida.mostrarString("INFORME:\n\n" 
                                 + "Facturacion total: $"+c.facturacion()
                                 + "\nPreparacion mas pedida: "+ prep
                                 + "\nBebida mas Pedida: " + beb
                                 + "\nCamarero con Mas pedidos: "+ c.masPedidos());
                         m.reestablecerCantidad();
                          c=null;
                         EntradaSalida.mostrarString("Reestablecidas cantidad de productos pedidos a 0");
                        
                         break;
            }
        }
        return seguirCorriendo;
    }

    public Menu getM() {
        return m;
    }

    public void setM(Menu m) {
        this.m = m;
    }

    public Pedidos getC() {
        return c;
    }

    public void setC(Pedidos c) {
        this.c = c;
    }

    public Personal getPe() {
        return pe;
    }

    public void setPe(Personal pe) {
        this.pe = pe;
    }
 public void altaCocinero(String usuario, String password) {
        Cocinero nuevoCocinero = new Cocinero(usuario, password, "Cocinero");// Cocinero(usuario, password);
        nuevoCocinero.setM(m);
        pe.agregarCocinero(nuevoCocinero);
        EntradaSalida.mostrarString(
                "USUARIO CARGADO \n "+usuario+"["+nuevoCocinero.getCargo()+"]\n");
        
    }


    private void altaCamarero(String usuario, String password) {
        Camarero nuevoCamarero = new Camarero(usuario, password,"Camarero");
        nuevoCamarero.setM(m);
        nuevoCamarero.setC(c);
        pe.agregarCamarero(nuevoCamarero);
        EntradaSalida.mostrarString(
                "USUARIO CARGADO \n "+usuario+"["+nuevoCamarero.getCargo()+"]\n");
    }
    private void altaBebida(String nom, float precio){
    Bebida nuevoBeb=new Bebida(nom, precio,0);
        m.agregarBebida(nuevoBeb);
        
    }
     boolean existeCodNoDisp(int buscar){
    boolean r=false;
    int aux;
 
        for( int i=0;i<m.obtenerCodNoDisp().size();i++){
            aux=(int) m.obtenerCodNoDisp().get(i);
          if(buscar==aux){
              r=true;
          }
        }
    
    return r;
    
    }

   
}
