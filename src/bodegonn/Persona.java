package bodegonn;

import java.io.Serializable;

public abstract class Persona implements Serializable {

    final String usuario;
    private String password;
    private String cargo;

    public Persona(String usuario, String password, String cargo) {
        this.usuario = usuario;
        this.password = password;
        this.cargo = cargo;
    }
   

    public String getUsuario() {
        return usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public abstract boolean proceder();
 
    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

}
