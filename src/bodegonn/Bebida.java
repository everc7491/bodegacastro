package bodegonn;

public class Bebida extends Producto{

    public Bebida(String nombre, float precio, int cantidad) {
        super(nombre, precio, cantidad);
        setTipo("BEBIDA");
        setCantidad(0);
    }
    
}
