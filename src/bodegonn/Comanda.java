package bodegonn;

public class Comanda {
private String pedido;
private float cuenta;
private String camarero; 

    public Comanda(String pedido, float cuenta, String camarero) {
        this.pedido = pedido;
        this.cuenta = cuenta;
        this.camarero= camarero;
    }

    public String getPedido() {
        return pedido;
    }

    public void setPedido(String pedido) {
        this.pedido = pedido;
    }

    public float getCuenta() {
        return cuenta;
    }

    public void setCuenta(float cuenta) {
        this.cuenta = cuenta;
    }

    public String getCamarero() {
        return camarero;
    }
}
