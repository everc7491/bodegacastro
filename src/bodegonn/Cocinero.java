package bodegonn;

public class Cocinero extends Persona{
private Menu m;

    public Cocinero(String usuario, String password, String cargo) {
        super(usuario, password, cargo);
        setCargo("Cocinero");
    }
   

    @Override
    public boolean proceder() {
        String nombre;
        boolean cerrar = false;
        while (!cerrar) {
            char op;
            do {
                op = EntradaSalida.leerChar(
                        "MENÚ COCINERO\n\n"
                        +"[0]CERRAR SESION\n"        
                        + "[1] Alta Preparacion");
            } while (op < '0' || op > '1');
            switch (op) {
                case '1': 
                    nombre=EntradaSalida.leerString("Detalle la preparacion");
                    Preparacion pre=  new Preparacion(nombre,0,0);
                    m.agregar(pre);
                    break;
                case '0':
                    cerrar = true;
                    EntradaSalida.mostrarString("¡Hasta pronto!");
                    break;
            }
        }
        return true;
    }

    public Menu getM() {
        return m; 
    }

    public void setM(Menu m) {
        this.m = m;
    }

   
    
}
